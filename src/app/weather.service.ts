import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  apiKey = 'e4739ffcc2ef4a849f7103101182711';
  url;

  constructor(public http: Http) {
    console.log('Hello weatherProvider');
    this.url = 'http://api.worldweatheronline.com/premium/v1/';
   }
   getWeather(query, format, extra, num_of_days, date, fx, cc, includelocation, show_comments) {
     // return this.http.get(this.url + '/'+state+'/'+city+'.json')
     // tslint:disable-next-line:max-line-length
     return this.http.get(this.url + 'weather.ashx?q=' + query + '&format=' + format + '&extra=' + extra + '&num_of_days=' + num_of_days + '&date=' + date + '&fx=' + fx + '&cc=' + cc + '&includelocation=' + includelocation + '&show_comments=' + show_comments + '&key=' + this.apiKey)
     .pipe(map((res) => res.json()));
   }
}
