import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/Storage';
import { NavController } from '@ionic/angular';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  query: string;
  num_of_days: string;
  time: any;

  constructor(public router: Router,   private storage: Storage, public navCtrl: NavController) {
this.storage.get('weatherInfo').then((val) => {
if (val != null) {
  const weatherInfo = JSON.parse(val);
  this.query = weatherInfo.query;
  this.num_of_days = weatherInfo.num_of_days;
  this.time = weatherInfo.time;
} else {
  this.query = 'Kortrijk';
  this.num_of_days = '1';
  this.time = 0;
}
});
  }

  ngOnInit() {
  }

  Save() {
    const weatherInfo = {
      query: this.query,
      num_of_days: this.num_of_days,
      time: this.time
    };
    this.storage.set('weatherInfo', JSON.stringify(weatherInfo));
    // window.location.reload();
    // this.navCtrl.navigateRoot(HomePage);
    this.router.navigateByUrl('/tabs/(home:home)');
    console.log(weatherInfo);
  }
}
