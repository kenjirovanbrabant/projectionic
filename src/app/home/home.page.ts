import { Component } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Storage } from '@ionic/Storage';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  Weather: any;
  weatherInfo: {
    query: string,
    num_of_days: string,
    time: any
  };
  timeInHours: string;
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private WeatherService: WeatherService, private storage: Storage) {
  }
  ionViewWillEnter() {
    this.storage.get('weatherInfo').then((val) => {
      if (val != null) {
        this.weatherInfo = JSON.parse(val);
      } else {
        this.weatherInfo = {
          query: 'Kortrijk',
          num_of_days: '1',
          time: 0
        };
      }

      this.WeatherService.getWeather(this.weatherInfo.query, 'JSON', '', this.weatherInfo.num_of_days,
        '', '', '', '', '')
        .subscribe(weather => {
          this.Weather = weather.data;
          console.log(this.Weather);
          console.log((Boolean)(weather.data.error ? weather.data.weather : 'Error'));
          if ((Boolean)(weather.data.error ? weather.data.weather : 'Error')) {
          this.storage.set('weatherData', JSON.stringify(weather.data));
          console.log(this.weatherInfo.time);
          // tslint:disable:triple-equals
          if (this.weatherInfo.time == 1) {
            this.timeInHours = '3 AM';
          } else if (this.weatherInfo.time == 2) {
            this.timeInHours = '6 AM';
               } else if (this.weatherInfo.time == 3) {
            this.timeInHours = '9 AM';
               } else if (this.weatherInfo.time == 4) {
            this.timeInHours = '12 AM';
               } else if (this.weatherInfo.time == 5) {
            this.timeInHours = '3 PM';
               } else if (this.weatherInfo.time == 6) {
            this.timeInHours = '6 PM';
               } else if (this.weatherInfo.time == 7) {
            this.timeInHours = '9 PM';
               } else {
            this.timeInHours = '0 AM';
               }
        } else {
          alert(weather.data.error[0].msg);
          }
        });
    });
  }
}
