import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/Storage';

@Component({
  selector: 'app-averages',
  templateUrl: './averages.page.html',
  styleUrls: ['./averages.page.scss'],
})
export class AveragesPage implements OnInit {
  weatherData: any;
  weatherInfoMonths: any;
  months: any;
  location: any;

  constructor(private storage: Storage) {
    this.storage.get('weatherData').then((val) => {
        this.weatherData = JSON.parse(val);
        this.location = this.weatherData.request[0] ? this.weatherData.request[0].query : '';
        this.weatherInfoMonths = this.weatherData.ClimateAverages[0].month;
        console.log(this.weatherInfoMonths);
    });
   }


  ngOnInit() {
  }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    for (let i = 0; i < 12; i++) {
      if (this.weatherInfoMonths[i].name === ev.detail.value) {
        this.months = this.weatherInfoMonths[i];
      }
    }
  }
}
