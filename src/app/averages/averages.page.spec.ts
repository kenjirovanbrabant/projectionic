import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AveragesPage } from './averages.page';

describe('AveragesPage', () => {
  let component: AveragesPage;
  let fixture: ComponentFixture<AveragesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AveragesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AveragesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
